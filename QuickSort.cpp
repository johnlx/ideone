#include <iostream>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <vector>
using namespace std;

#define RAND_NUMS 1000

void QuickSort(vector<int> &input_vector, int begin, int end)
{
	if(begin >= end) return;
	
	int first=begin;
	int last=end;
	int key=input_vector[first];
	
	while(first < last) {
		while(input_vector[last] >= key && last > first) last--;
		input_vector[first] = input_vector[last];
		
		while(input_vector[first] <= key && first < last) first++;
		input_vector[last] = input_vector[first];
	}
	
	input_vector[first] = key;
	
	QuickSort(input_vector, begin, first - 1);
	QuickSort(input_vector, last + 1, end);
}

bool algorithm(vector<int> &input_vector)
{
	QuickSort(input_vector, 0, input_vector.size());
	return true;
}

void check_show_vector(const vector<int> input_vector)
{
	for (int i=0; i<input_vector.size() - 1; i++) {
		if(input_vector[i] > input_vector[i+1]) {
			cout << "Error at index " << i << " :" << input_vector[i] << " " << input_vector[i+1] << "\n";
		}
	}
	
	for (int i=0; i<input_vector.size(); i++)
		cout << " " << input_vector[i];

	cout << "\n";
}

int main() {
	// your code goes here
	clock_t begin_time;
	clock_t consume_time;
	
	/* initialize random seed: in order to generate different rand data everytime
	 * check on: www.cplusplus.com
	 */
	srand (time(NULL));
	vector<int> int_vector;
	
	for(int i=0; i<RAND_NUMS; i++)
		int_vector.push_back(rand()%RAND_NUMS);
	
	begin_time = clock();

	algorithm(int_vector);
	
	consume_time = clock() - begin_time;
	
	cout << "consume clock is " << (consume_time) << "\n";
	cout << "consume time is " << ((float)consume_time)/CLOCKS_PER_SEC << "\n";
	
	check_show_vector(int_vector);
	return 0;
}