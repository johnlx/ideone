#include <iostream>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <vector>
using namespace std;

#define RAND_NUMS 1000
/* merge(): merge the 2 ordered array1[being ... mid], array2[mid+1 ... end] to one array 
 *	input_vector: the array needed to sort, the content should be editable by this function
 *	begin       : begin of array1
 *	mid         : end of array1
 *		mid+1   : begin of array2
 *	end         : end of array2
 * 
 */
void merge(vector<int> &input_vector, int begin, int mid, int end)
{
	int array1_begin=begin;
	int array1_end=mid;
	int array2_begin=mid+1;
	int array2_end=end;
	
	vector<int> temp_vector;
	
	while(array1_begin <= array1_end &&
	      array2_begin <= array2_end) {
		if(input_vector[array1_begin] <= input_vector[array2_begin]) {
			temp_vector.push_back(input_vector[array1_begin++]);
		} else {
			temp_vector.push_back(input_vector[array2_begin++]);
		}
	}
	
	while(array1_begin <= array1_end)
		temp_vector.push_back(input_vector[array1_begin++]);
		
	while(array2_begin <= array2_end)
		temp_vector.push_back(input_vector[array2_begin++]);
		
	for (vector<int>::iterator it = temp_vector.begin();
	     it != temp_vector.end();
	     ++it) {
		input_vector[begin++] = *it;
	}
	     
}

/* MergeSort(): split array from the mid, until there is only 2 items in the array, then merge 
 *	input_vector: the array needed to sort, the content should be editable by this function
 *	begin       : begin of array need to be sort.
 *	end         : end of array need to be sort.
 * 
 */
void MergeSort(vector<int> &input_vector, int begin, int end)
{
	if(begin < end) {
		int mid = (begin + end) / 2;
		MergeSort(input_vector, begin, mid);
		MergeSort(input_vector, mid+1, end);
		merge(input_vector, begin, mid, end);
	}
}

bool algorithm(vector<int> &input_vector)
{
	MergeSort(input_vector, 0, input_vector.size());

	return true;
}

void check_show_vector(const vector<int> input_vector)
{
	for (int i=0; i<input_vector.size() - 1; i++) {
		if(input_vector[i] > input_vector[i+1]) {
			cout << "Error at index " << i << " :" << input_vector[i] << " " << input_vector[i+1] << "\n";
		}
	}
	
	for (int i=0; i<input_vector.size(); i++)
		cout << " " << input_vector[i];

	cout << "\n";
}

int main() {
	// your code goes here
	clock_t begin_time;
	clock_t consume_time;
	
	/* initialize random seed: in order to generate different rand data everytime
	 * check on: www.cplusplus.com
	 */
	srand (time(NULL));
	vector<int> int_vector;
	
	for(int i=0; i<RAND_NUMS; i++)
		int_vector.push_back(rand()%RAND_NUMS);
	
	begin_time = clock();

	algorithm(int_vector);
	
	consume_time = clock() - begin_time;
	
	cout << "consume clock is " << (consume_time) << "\n";
	cout << "consume time is " << ((float)consume_time)/CLOCKS_PER_SEC << "\n\r";
	
	check_show_vector(int_vector);
	return 0;
}